var async = require('async');
var util = require('../utilities.js');
module.exports = function(LeaveWorkflow){

	LeaveWorkflow.afterRemote('**',function(context, user, next){		
		console.log("-----------------------------",context.methodString);
		next();
	});


	LeaveWorkflow.getLeaveWorkflows = function(id, cb){												//API to get the leave workflow attached with all leave request of an employee 
		var data = [];

		async.waterfall([
		function actionTakenValue(callback){														//function to get all the key_codes for action_taken(approve,deny or send back) 
			util.ErpKeysCode("action_taken", null
			,function(err, actionTakenValues){
				if(err){
					return callback(err);
				}
				else{
					callback(null, actionTakenValues);
				}
			});
		},

		function getWorkflow(actionTakenValues ,callback){											//function to get the leaveworkflow wrt a leave id
			LeaveWorkflow.find({
			where:{leaveRequestId: id},
			include: {
				relation: 'employee',
				scope:{
					fields: ['name', 'firstName', 'lastName', 'empId', 'reportingManagerId']
				}
			},
			order: 'actionTimestamp DESC'
			}
			,function(err, leaveWorkflows){
				if(err){
					return callback(err);
				}
				else{
					callback(null, actionTakenValues, leaveWorkflows);
				}
			});
		},

		function modellingWorkflow(actionTakenValues, leaveWorkflows, callback){					 //formatting leave workflow to be send 
			var workflow = {};
			async.forEachOf(leaveWorkflows
			,function(leaveWorkflow, index, cb){
				console.log(leaveWorkflow);
				workflow = {};
				workflow.fromEmployee = leaveWorkflow.employee().name;
				workflow.fromEmployeeFirstName = leaveWorkflow.employee().firstName;
				workflow.fromEmployeeLastName = leaveWorkflow.employee().lastName;
				workflow.toEmployee = leaveWorkflow.toEmployee;
				workflow.actionTaken = leaveWorkflow.actionTaken;
				workflow.actionTimestamp = leaveWorkflow.actionTimestamp;

				if(leaveWorkflow.comments!==" "){
					workflow.comments = leaveWorkflow.comments;
				}
				if((leaveWorkflow.employee().empId === "P10E0001")||(leaveWorkflow.employee().empId === "P10E0002")||(leaveWorkflow.employee().empId === "P10E0008")){
					workflow.lastApprover = 1;
				}
				actionTakenValues.forEach(function(actionTakenValue){
					if(actionTakenValue.keyCode === workflow.actionTaken){
						workflow.actionTaken = actionTakenValue.keyValue;
					}
				});
				data.push(workflow);
				cb(null);
			}
			,function(err){																				//function returning from async.forEachOf
				if(err){
					return callback(err);
				}
				else{
					callback(null);
				}
			});
		}
		]
		,function(err, response){																		//function returning from getLeaveWorkflows method
			if(err){
				return cb(err);
			}
			else{		
				cb(null, data);
			}
		});

	};


	LeaveWorkflow.remoteMethod('getLeaveWorkflows',									//Registering the getLeaveWorkflows Method in the list of Api's
	 	{
	 		http :{path:'/getLeaveWorkflows', verb: 'get'},
	 		accepts:{arg:'id', type: 'string', required: true},
	 		returns: {type: 'string', root: true }
	 	}
	);

/*
var data = {															
			"fromEmployee": req.employeeId,				
			"toEmployee": req.approverId,
			"actionTaken": key.keyCode,
			"actionTimestamp": currentDate,
			"comments": req.comments,
			"leaveRequestId": req.id
			};

*/



	LeaveWorkflow.observe('after save', function(ctx, next){						//function to change the status of leave request after last level manager has taken action on a particular leave request
		var req = ctx.instance; 													//or a particular manager has denied the leave request
		console.log("new feature",req);
		var leaveStatus;
		var  leaveStatusCode;
		if((req.actionByManager=== "Approve" && req.lastApprover=== 1) || (req.actionByManager=== "Deny")){	
			async.waterfall([
			function findLeaveStatus(callback){
				if(req.actionByManager=== "Approve"){
					leaveStatus = "Approved";
				}
				else{
					leaveStatus = "Denied";
				}
				util.ErpKeysCode("leave_status", leaveStatus
				,function(err, res){												//function to find the leave status code for approved or denied
					if(err){
						return callback(err);
					}
					else{
						leaveStatusCode = res.keyCode;
						callback(null);
					}
				});	
			},

			function leaveStatusUpdate(callback){									//function to update the leave status in the leave request table
				app.models.LeaveRequest.updateAll({
				id: req.leaveRequestId},{status: leaveStatusCode}
				,function(err, response){
					if(err){
						return callback(err);
					}
					else{
						callback(null);
					}
				});
			},

			function specialLeaveUpdate(callback){									//function to update the special leave table if action taken is deny
				if(leaveStatus === "Denied"){
					app.models.EmployeeSpecialLeaveEligibility.updateAll(
					{leaveRequestId:req.leaveRequestId}, {status: 0, leaveRequestId: null} ,function(err, response){
						if(err){
							return callback(err);
						}
						else{
							callback(null);
						}
					});
				}
				else{
					callback(null);
				}
			}
			]
			,function(err, response){												 //function returning from leaveWorkflow after save											
				if(err){
					next(err);
				}
				else{
					next();
				}
			});
		}
		else{
			next();
		}
	});

};
