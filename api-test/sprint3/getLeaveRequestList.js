var request = require('supertest');
var async = require('async');
var chai = require('chai');
var expect = chai.expect;

describe('GET /leave-requests',function(){
	beforeEach(function(){
		request = request('http://localhost:5000/api')
	});
	afterEach(function(){

	});

	it('should get the leave-requests', function(done){
		async.waterfall([
			function applyNewLeaveRequest(callback){
				request
				.post('/leave_requests')                                          
				.send({fromDate: '2016-05-28T00:00:00.000Z',
						toDate: '2016-05-29T00:00:00.000Z',
						numberOfDays: '2',
  						description: 'sick leave',
  						type: 'Privilege',
  						employeeId: 'P1012346',
  						halfDaySlot: '1',
  						approverId: 'P1012345'
  					})
				.expect(200, function(err, res){
					var leave = JSON.parse(res.text);
					callback(null, leave.employeeId, leave.id);
				});	
			},
			function getNewlyAppliedLeaveRequest(employeeId, id, callback){
				request
				.get('/leave_requests/getList')
				.send({id: 'P1012346'})
				.expect(200, function(err, res){
					var leave = JSON.parse(res.text);
					var len = leave.length;
					expect(leave[len-1].fromDate).to.equal('2016-05-28T00:00:00.000Z');
					expect(leave[len-1].toDate).to.equal('2016-05-29T00:00:00.000Z');
					expect(leave[len-1].noOfdays).to.equal(2);
					expect(leave[len-1].typeOfLeave).to.equal('Privilege');
					callback(null, id);
				})
			},
			function deleteLeaveRequest(id, callback){
				request
				.delete('/leave_requests/' + id)
				.expect(200, function(err, res){
					console.log(res.status);
					expect(res.status).to.equal(200);
					callback();
				});
			}
			],function(err){
				done();
			});
	})
});
