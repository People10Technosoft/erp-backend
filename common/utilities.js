var async = require('async');
var ejs = require('ejs');
var fs = require('fs');
var loopback = require('loopback');
var moment = require('moment');
require('moment-weekday-calc');

var date = function(fromDate, toDate, numberOfDays, cb){						//To calculate the no. of days /to-date from from-date
	var days = 0;
	console.log("%%%%%%%%%%",fromDate,toDate,numberOfDays);
	if((fromDate == "Invalid Date") || (numberOfDays === undefined))
	{
		var arr = [];
		var toDateString = toDate.toISOString();
		var count = 0;
		var day = toDate.getDay();
		console.log("@@@@@@@@@@@@@@",day);
		app.models.HolidayList.find({where: {holidayDate: {lte: toDate}}
		},function(err, leaves){
			if(err)
				return cb(err);

			leaves.forEach(function(leave){
				if(leave.holidayDate.toISOString()==toDateString)
					count++;
				arr.push(leave.holidayDate);
			});

			console.log(count);
			if(fromDate == "Invalid Date")
			{
				if(count!=0 || day==0 || day==6){
					fromDate = moment(toDate).addWorkdays(-numberOfDays,arr)._d;	
				}
				else{
					fromDate = moment(toDate).addWorkdays(-(numberOfDays-1),arr)._d;
				}
			var data ={
						fromDate: fromDate
			};
			}
			else
			{
				days = moment().weekdayCalc(fromDate, toDate, [1,2,3,4,5] ,arr);
				var data = {
						numberOfDays: days
				};
			}
			console.log(data);
		cb(null, data);
		});
	}
	else{
		var arr = [];
		var fromDateString = fromDate.toISOString();
		var count = 0;
		var day = fromDate.getDay();
		console.log("**************",day);
		app.models.HolidayList.find({
		},function(err, leaves){
			if(err)
				return cb(err);

			leaves.forEach(function(leave){
				if(leave.holidayDate.toISOString()==fromDateString)
					count++;
				arr.push(leave.holidayDate);
			});

			console.log(count);
			//console.log(arr);
			var data = {};
			if(count!=0 || day==0 || day==6){
				if(numberOfDays == 0.5)
				{
					toDate = fromDate ;
					data.halfDay = 1;
				}
				else
					toDate = moment(fromDate).addWorkdays(numberOfDays,arr)._d;	
			}
			else{
				if(numberOfDays == 0.5)
				{
					toDate = fromDate ;
					data.halfDay = 1;
				}
				else
				toDate = moment(fromDate).addWorkdays(numberOfDays-1,arr)._d;
			}

			data.toDate = toDate ;
			console.log(data);
			cb(null, data);
		});
	}
};


var specialLeaveDateCalculation = function(fromDate, toDate, numberOfDays, callback){                    //function to calculate the to-date/number of days from from-date field
	var data = {};
	console.log(fromDate, toDate, numberOfDays);
	var toDateString = toDate.toString(); 																						 //in this case holidays and weekends will be included
	console.log(fromDate, toDateString, numberOfDays);	
	if(numberOfDays === undefined){
		var days = moment().weekdayCalc(fromDate, toDate, [0,1,2,3,4,5,6]);
		data.numberOfDays = days;
	}
	else if(toDateString === "Invalid Date"){
		toDate = moment(fromDate).addWeekdaysFromSet(numberOfDays-1, [0,1,2,3,4,5,6]);
		data.toDate = toDate ;
	}
	callback(null, data);
};


var ErpKeysCode = function(type, keyValue, cb){															//function to fetch the key values from erp_keys table
	if(keyValue === null){
		app.models.ErpKeys.find({where: {type: type}
		},function(err, keys){
			if(err){
				return cb(err);
			}
			else{
				cb(null, keys);
			}
		});
	}
	else{
		app.models.ErpKeys.findOne({where: {and: [{type: type}, {keyValue: keyValue}]}
		},function(err, key){
			if(err){
				return cb(err);
			}
			else{
				cb(null, key);
			}
		});
	}
};

	
var sortWorkflow = function(arr, cb){																	//function to sort the leave workflow
	arr.sort(function(a, b){
		return b.actionTimestamp - a.actionTimestamp;
	});
	cb(null, arr);
};



var latestTimestamp = function(id, callback){																//function to get the latest leave_workflow entry 
	var pgp = require("pg-promise")(/*options*/);
	var db = pgp("postgres://dbOwnerAdmin:mayank@10.0.1.84:5433/erp_lms");								//here we have used native query as group by is not there in loopback

	db.manyOrNone("SELECT leave_request_id, MAX(action_timestamp) AS action_timestamp FROM leave_workflow GROUP BY leave_request_id ORDER BY action_timestamp ASC")
  	.then(function (data) {
  		//console.log("*********successfully done;",data);
        callback(null, data);
  	})
  	.catch(function (error) {
      console.log("ERROR:", error);
      callback(error);
  	});

};


var dateConflicts = function(req, callback){															//function to validate the leave request when a user applies a leave
	var fromDate = new Date(req.fromDate);
	var fromDate_new = new Date(fromDate.toDateString());
	var toDate = new Date(req.toDate);
	var toDate_new = new Date(toDate.toDateString());
	var count_privilege = 0;
	var count_total = 21;
	var count_date_match = 0;
	var leaveId;
	var typeOfLeaveRequest = req.type;
	console.log("###############",req.type);
	var cancelStatus;
	var deniedStatus;
	var eligible_leave_count;

	if(req.id === undefined){
		leaveId = null;
	}
	else{
		leaveId = req.id;
	}

	async.waterfall([
	function cancelStatusValue(callback){																 //function to get the cancel status key value 
		app.models.ErpKeys.findOne({where: {and: [{type: "leave_status"}, {keyValue: "Cancelled"}]}
		},function(err, res){
			if(err){
				callback(err);
			}
			else{
				cancelStatus = res.keyCode;
				callback(null);
			}
		});
	},

	function deniedStatusValue(callback){																//function to get the deny status key value 
		ErpKeysCode("leave_status", "Denied"
		,function(err, res){
			if(err){
				callback(err);
			}
			else{
				deniedStatus = res.keyCode;
				callback(null);
			}
		});
	},

	function findEligibleLeaveCount(callback){
		app.models.EmployeePrivilegeLeaveEligibility.findOne({where: {employeeId: req.employeeId}},function(err, leaveCount){
			if(err){
				callback(err);
			}
			else{
				console.log("leaveCount", leaveCount.eligibility);
				eligible_leave_count = leaveCount.eligibility;
				callback(null);
			}
		});
	},

	function checkingForDateConflicts(callback){														//function to check for date conflicts
		app.models.LeaveRequest.find({
		where: {and: [{employeeId: req.employeeId}, {status:{nin: [cancelStatus,deniedStatus]}}]},
		include: {relation: 'typeOfLeaves'}}
		,function(err, response){
			if(err){
				return callback(err);
			}
			var res_fromDate;
			var fromDate_db;
			var res_toDate;
			var toDate_db;
			var check_1;
			var check_2;
			var check_3;
			var check_4;
			response.forEach(function(res){																//to loop through all the leave requests to check for date matching 
				if(res.id === leaveId){
					console.log("Do nothing");
				}
				else{
					res_fromDate = new Date(res.fromDate);
					fromDate_db = new Date(res_fromDate.toDateString());
					res_toDate = new Date(res.toDate);
					toDate_db = new Date(res_toDate.toDateString());
					check_1 = moment(fromDate_new).isBefore(fromDate_db);
					check_2 = moment(toDate_new).isBefore(fromDate_db);
					check_3 = moment(fromDate_new).isAfter(toDate_db);
					check_4 = moment(toDate_new).isAfter(toDate_db);
					if((check_1&&check_2) || (check_3&&check_4)){}
					else{
						count_date_match++;	
					}
					if(res.typeOfLeaves().leaveType === "Privilege"){
						count_privilege+=res.numberOfDays;
					}
				}
			});
			if( ((count_privilege + req.numberOfDays) > eligible_leave_count) && (req.type === "Privilege") ){			//validation for privilege leave count
				return callback("You have only " + (eligible_leave_count-count_privilege) + " privilege leave available");
			}
			if(count_date_match !== 0){
				return callback("Leave has been taken already for the same date");
			}
		//	console.log("inside util 2nd phase", req);
			callback(null);
		});
	},

	function typeOfLeaveCode(callback){																  //function to get the leave type key value
			app.models.TypeOfLeaves.findOne({where: {leaveType: typeOfLeaveRequest}
			},function(err, leave){
				if(err){
					callback(err);
				}
				else{
					req.typeOfLeavesId = leave.id;
					//console.log("inside util 3rd phase", req);
					callback(null);
				}
			});
	},
	function specialLeaveEligibility(callback){														  //function to check conflicts wrt special leaves
		if((typeOfLeaveRequest === "Paternity")||(typeOfLeaveRequest === "Maternity")||(typeOfLeaveRequest === "Comp-off")){
			//if(req.numberOfDays)
			app.models.EmployeeSpecialLeaveEligibility.find({										  //validte if the applied leave type is special leave
			where: {and: [{employeeId: req.employeeId},{typeOfLeavesId: req.typeOfLeavesId}, {status:{neq: 1}}]}
			},function(err, specialLeaves){
			//	console.log(specialLeaves);
				if(err){
					return callback(err);
				}
				if(specialLeaves.length > 0){
					if((req.type === "Comp-off")&&(req.numberOfDays > specialLeaves.length)){
						return callback("You have only " +  specialLeaves.length +" compensatory leave available");
					}
					else if((req.type === "Comp-off")&&(!Number.isInteger(req.numberOfDays))){
						return callback("COMP OFF LEAVE SHOULD BE AN INTEGER");
					}
					else if((req.type === "Paternity")&&(req.numberOfDays > 5)){
						return callback("You can take only 5 days for Paternity leave");
					}
					else if((req.type === "Maternity")&&(req.numberOfDays > 90)){
						return callback("You can take only 90 days for Maternity leave");
					}
					else{
						callback(null);
					}
				}
				else{
					return callback("NO SPECIAL LEAVES ALLOTED YET ");	
				}
			});
		}		
		else{
			//console.log("inside util 4th phase", req);
			return callback(null);
		}
	}
	]
	,function(err, response){
		if(err){
			return callback(err);
		}
		else{
			callback(null, req);
		}
	});

};



var updateSpecialLeaves = function(req, clb){													//function to update employee's leave data in case of special leaves
	var typeOfLeaveRequest = req.type;

	async.waterfall([
	function getAvailableSpecialLeaves(callback){
		if((typeOfLeaveRequest === "Paternity")||(typeOfLeaveRequest === "Maternity")||(typeOfLeaveRequest === "Comp-off")){
			app.models.EmployeeSpecialLeaveEligibility.find({									//function to get all the special leaves not taken by an employee
			where: {and: [{employeeId: req.employeeId},{typeOfLeavesId: req.typeOfLeavesId}, {status:{neq: 1}}]}
			},function(err, specialLeaves){
				if(err){
					return callback(err);
				}
				else{
					callback(null, specialLeaves);	
				}
			});
		}
		else{
			callback(null, null);
		}
	},

	function updateSpecialLeaveStatus(specialLeaves, callback){									//function to employee's special leave status field
		var requestedDays = req.numberOfDays;
		var count = 0;
		//console.log("specialLeaves", specialLeaves, requestedDays);
		if(specialLeaves !== null){
			if(requestedDays < 1){
				callback("SPECIAL LEAVES SHOULD BE IN INTEGER FORM");
			}
		//	console.log("start");
			if(typeOfLeaveRequest === "Comp-off"){												//to loop through so that applied comp off leaves status can be changed
				async.whilst(
				function(){ return requestedDays > 0;},
				function(cb){
					requestedDays -= 1;
					app.models.EmployeeSpecialLeaveEligibility.updateAll(
					{id:specialLeaves[count].id},{leaveRequestId: req.id, status: 1},function(err, response){
						if(err){
							return cb(err);
						}
						else{
							count++;
							cb(null);
						}
					});			
				},
				function(err, n){
					if(err){
						callback(err);
					}
					else{
						callback(null);
					}
				}
				);
			}
			else{
				app.models.EmployeeSpecialLeaveEligibility.updateAll(							//if it is not comp off it must be either paternity or comp off
				{id:specialLeaves[0].id},{leaveRequestId: req.id, status: 1},function(err, response){
					if(err){
						return callback(err);
					}
					else{
						callback(null);
					}
				});
			}
		}
		else{
			callback(null);
		}
		}
	]
	,function(err, response){																	//function returning result to calling function
		if(err){
			clb(err);
		}
		else{
			clb(null);
		}
	});
};



var sendEmailNotificationToEmployee = function(emailData, cb){									//function to send email notification to the employee by manager in case of approve,sendback or deny
	console.log("Email data:- ", emailData);
	var str = fs.readFileSync(__dirname + '/../server/views/emailNotificationToEmployee.ejs', 'utf8');
	var subject = "Action regarding your leave request:- From Date:" + emailData.leaveFromDate  + " To Date:" + emailData.leaveToDate ;
	var html = ejs.render(str, emailData);

    app.models.Email.send({																		//query to send the email
    	to: emailData.employeeEmail,
      	from: 'Manager',
      	subject: subject,
     	text: "Hello",
      	html: html
    }
    ,function(err, mail){
    	if(err){
    		cb(err);
    	}
    	else{
      		console.log('email sent!');
      		cb(null,"email sent");
      	}
    });
  };


  var sendEmailNotificationToManager = function(emailData, cb){									//function to send email notification to the manager in case of cancel or update
	console.log("Email data:- ", emailData);
	var str = fs.readFileSync(__dirname + '/../server/views/emailNotificationToManager.ejs', 'utf8');
	var subject = "Leave Request:- From Date:" + emailData.leaveFromDate  + " To Date:" + emailData.leaveToDate ;
	var html = ejs.render(str, emailData);
	
    app.models.Email.send({																		//query to send the email notification
    	to: emailData.managerEmail,
      	from: 'Employee',
      	subject: subject,
     	text: "Hello",
      	html: html
    }
    ,function(err, mail) {
    	if(err){
    		cb(err);
    	}
    	else{
      		console.log('email sent!');
      		cb(null,"email sent");
      	}
    });
 };


var leaveApplicationNotification = function(emailData, cb){										//function to send an email notification to manager in case of an employee applies for a leave request
	console.log("Email data:- ", emailData);
	var str = fs.readFileSync(__dirname + '/../server/views/applyLeave.ejs', 'utf8');
	var subject = "Leave Request:- From Date:" + emailData.leaveFromDate  + " To Date:" + emailData.leaveToDate ;
	var html = ejs.render(str, emailData);
	
    app.models.Email.send({																		//query to send the email notification
    	to: emailData.managerEmail,
      	from: 'Employee',
      	subject: subject,
     	text: "Hello",
      	html: html
    }
    ,function(err, mail){
    	if(err){
    		cb(err);
    	}
    	else{
      		console.log('email sent!');
      		cb(null,"email sent");
      	}
    });
 };


var loginStatus = function(id, callback){														//function to check whether employee login for the firest time
	app.models.employee.findOne({where: {id: id},
	fields: {loginStatus: true}
	},function(err, employeeData){	
		if(err){
			return callback(err);
		}
		if(employeeData.loginStatus === '0'){
			return callback(null,0);
		}
		else{
			return callback(null,1);
		}
	});
};


var managerList = function(callback){															//function to find all the managers in an organization
	app.models.Employee.find({where: {reportingManagerId: {neq: null}},
	fields : {reportingManagerId: true}
	}, function(err, managerList){
		if(err){
			callback(err);
		}
		else{
			callback(null, managerList);
		}
	});
};

var userIdentification = function(cb){															//function to find the current user based on the access token
    var ctx = loopback.getCurrentContext();
    var currentUser = ctx && ctx.get('currentUser');
    if(currentUser === undefined){
    	return cb(new Error("NOT A VALID USER"));
    }
  	else{
    	cb(null, currentUser);
  	}
};


exports.userIdentification = userIdentification ;
exports.managerList = managerList ;
exports.loginStatus = loginStatus ;
exports.leaveApplicationNotification = leaveApplicationNotification;
exports.sendEmailNotificationToEmployee = sendEmailNotificationToEmployee ;
exports.sendEmailNotificationToManager = sendEmailNotificationToManager ;
exports.updateSpecialLeaves = updateSpecialLeaves;
exports.dateConflicts = dateConflicts;	
exports.latestTimestamp = latestTimestamp;
exports.sortWorkflow =sortWorkflow;
exports.ErpKeysCode = ErpKeysCode;
exports.specialLeaveDateCalculation = specialLeaveDateCalculation;
exports.date = date ;