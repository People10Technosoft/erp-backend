var async = require('async');
var util = require('./utilities.js');

var leaveOptions =function(id, cb){																		//function to fetch all the leave options an employee has
	var currentDate = new Date();
	var currentYear = currentDate.getFullYear();
	var privilegeEligibility = 0; 
	var cancelStatus;
	var deniedStatus;
	var types = [];

	async.waterfall([
	function privilegeEligibilityCount(callback){														//function to get the eligible privilege leave count for a particular employee
		app.models.TypeOfLeaves.findOne({
		where: {leaveType: "Privilege"}
		},function(err, leaveType){
			if(err){
				return callback(err);
			}
			else{
				privilegeEligibility = leaveType.eligibility ;
				callback(null);
			}
		});
	},

	function cancelStatusValue(callback){																//function to get the cancel status from key table
		util.ErpKeysCode("leave_status", "Cancelled", function(err, res){
			if(err){
				return callback(err);
			}
			else{
				cancelStatus = res.keyCode;
				callback(null);
			}
		});	
	},

	function deniedStatusValue(callback){																//function to get the denied status from key table
		util.ErpKeysCode("leave_status", "Denied", function(err, res){
			if(err){
				callback(err);
			}
			else{
				deniedStatus = res.keyCode;
				callback(null);
			}
		});
	},

	function getAllLeaveRequests(callback){																//function to get all leave requests by an employee  
		app.models.LeaveRequest.find({
		where: {and: [{employeeId:id}, {leaveRequestYear:currentYear}, {status: {nin: [cancelStatus, deniedStatus]}}]}
		,include: {relation: 'typeOfLeaves'}
		},function(err, leaves){
			if(err){
				return callback(err);
			}
			else{
				callback(null, leaves);
			}	
		});
	},

	function privilegeLeavesCount(leaves, callback){													//function to get the privilege leaves count applied by an employee
		var count_privilege = 0;
		var count_remaining = privilegeEligibility;
		var leaveType = {};
		if(leaves.length < 1){
			leaveType = {};
			leaveType.name = "privilege";
	 		leaveType.count = count_remaining;
	 		types.push(leaveType);
			return callback(null);
		}
		leaves.forEach(function(leave){																	//for-loop to loop through all the leaves
			if((leave.typeOfLeavesId === leave.typeOfLeaves().id)&&(leave.typeOfLeaves().leaveType === "Privilege")){
				count_privilege+=leave.numberOfDays;
			}
		});
		count_remaining =  count_remaining - (count_privilege);
		if(count_remaining > 0){
			leaveType = {};
			leaveType.name = "privilege";
	 		leaveType.count = count_remaining;
	 		types.push(leaveType);
		}
		callback(null);
	},

	function getSpecialTypes(callback){																	//function to get all the special leaves an employee is eligible for
		app.models.EmployeeSpecialLeaveEligibility.find({
		where: {and:[{employeeId: id}, {status: 0}]},
		include: {relation: 'typeOfLeaves'}}, function(err, leaveEligible){
			if(err){
				return callback(err);
			}
			var count_compoff = 0;
			var leaveType = {};
			if(leaveEligible.length < 1){
				return callback(null,null);
			}
	 		leaveEligible.forEach(function(leavetypes){													//for-loop to loop through all the special leaves an employee is eligible 
	 			if(leavetypes.typeOfLeaves().leaveType === "Comp-off"){
	 				count_compoff++;																	//to find the different category in it
	 			}
	 			if(leavetypes.typeOfLeaves().leaveType === "Paternity"){
	 				leaveType = {};
	 				leaveType.name = "paternity";
	 				leaveType.count = leavetypes.typeOfLeaves().eligibility;
	 				types.push(leaveType);
	 			}
	 			else if(leavetypes.typeOfLeaves().leaveType === "Maternity"){
	 				leaveType = {};
	 				leaveType.name = "maternity";
	 				leaveType.count = leavetypes.typeOfLeaves().eligibility;
	 				types.push(leaveType);
	 			}
	 		});
	 	if(count_compoff > 0){
	 		leaveType = {};
	 		leaveType.name = "Comp-off";
	 		leaveType.count = count_compoff;
	 		types.push(leaveType);
	 	}
	 	callback(null, types);
		});	
	}
	]
	,function(err, response){																			//returning final result to the calling function
		if(err){
			return cb(err);
		}
		else{
			cb(null, types);
		}
	});
};

exports.leaveOptions = leaveOptions ;
