var async = require('async');
var util = require('./utilities.js');
var moment = require('moment');
require('moment-weekday-calc');
/*
Data to be send:-
[{
        "fromEmployee": "3",
        "comments": "sadsadfdf",
        "leaveRequestId": "161",
        "actionByManager": "Approve",
        "employeeId":”4"
}];
*/



var managerActions = function(actions, callback){                                                          //functions to track the managers action
	var currentDate = new Date().toLocaleString();

	async.forEachOf(actions, function(action, index, cb){
		action.actionTimestamp = currentDate;
		var actionToLeaveRequest = action.actionByManager;

		async.waterfall([
		function actionTakenCode(callback){																   //function to get the action taken code from the key table
			util.ErpKeysCode("action_taken", action.actionByManager, function(err, res){
				if(err){
					return callback(err);
				}
				else{
					var actionTakenCode = res.keyCode;
					console.log("adiFirstPhase",actionTakenCode);
					callback(null, actionTakenCode);
				}
			});
		},

		function settingToEmployeeField(actionTakenCode, callback){										   //function to set the to Employee field(cases:-when a manager hasn't/has any reporting manager)
			action.actionTaken = actionTakenCode ;
			if(action.actionByManager === "Approve"){
				app.models.Employee.findOne({
				where: {id: action.fromEmployee},
				fields: {empId: true, reportingManagerId: true}}, function(err, employeeData){
					if(err){
						return callback(err);
					}
					managerName = employeeData.name;
					if((employeeData.empId === "P10E0001")||(employeeData.empId === "P10E0002")||(employeeData.empId === "P10E0008")){
					 	action.toEmployee = action.employeeId;
				 		action.lastApprover = 1;
					}
					else{
				 		action.toEmployee = employeeData.reportingManagerId;
					}
					if(action.comments === ""){
						action.comments = " ";
					}
					//console.log("2ndphase",action);
					callback(null);
				});
			}
			else if((action.actionByManager === "Deny") || (action.actionByManager === "Sendback")){
				action.toEmployee = action.employeeId ;
				console.log("adiFirstPhase",action.toEmployee);
				callback(null);
			}
		},

		function makeEntryInLeaveWorkflow(callback){													   //function to add an leave workflow entry when a manager takes an action
			console.log("3rdphase:-",action);
			app.models.LeaveWorkflow.create(action , function(err, obj){
				if(err){
					console.log("Hey It's not working");
					console.log(err);
			 		callback(err);
			 	}
				else{
			 		callback(null);
			 	}
			});
		},

		function getManagerDetails(callback){															  //function to get the manager details (who is taking action)
			app.models.Employee.findOne({
			where: {id: action.fromEmployee},
			fields: {reportingManagerId: true ,name: true, empId: true, designation: true, email: true}}, function(err, managerData){
				if(err){
					return callback(err);
				}
				else{
					callback(null, managerData);
				}
			});					
		},

		function getEmployeeDetails(managerData, callback){												  //function to get the employee details (who has apllied this leave)
			app.models.Employee.findOne({
			where: {id: action.employeeId},
			fields: {email: true ,name: true, designation: true}}, function(err, employeeData){
				if(err){
					return callback(err);
				}
				else{
					callback(null, employeeData, managerData);
				}
			});					
		},

		function getLeaveRequest(employeeData, managerData, callback){									 //function to get the leave request details
			app.models.LeaveRequest.findOne({
			where: {id: action.leaveRequestId}}, function(err, leave){
				if(err){
					return callback(err);
				}
				else{
					callback(null, employeeData, managerData, leave);
				}
			});
		},
		function sendEmailNotification(employeeData, managerData, leave, callback){						//function to send the notification to an employee about action taken on his/her leave request
			var emailData = {
					actionByManager: actionToLeaveRequest,
					managerId: action.fromEmployee,
					managerEmpId: managerData.empId,
					managerName: managerData.name,
					managerEmail: managerData.email,
					employeeEmail: employeeData.email,
					managerDesignation: managerData.designation,
					comments: action.comments,
					leaveFromDate: moment(leave.fromDate).format("dddd, MMMM Do YYYY"),
					leaveToDate: moment(leave.toDate).format("dddd, MMMM Do YYYY")
			};
			util.sendEmailNotificationToEmployee(emailData, function(err, response){
				if(err){
					return callback("Email Notification Failed");
				}
				else{
					console.log("Email Notification Sent");
				}
			});
			callback(null);
		}
		]
		,function(err, response){																		//function returning results from async waterfall
			if(err){
				return cb(err);
			}
			else{
				cb(null);
			}
		});	
	}
	,function(err){																						//function returning results from async forEach to calling funcion
		if(err){
			return callback(err);
		}
		else{
			callback(null);
		}
	});
};

exports.managerActions = managerActions;