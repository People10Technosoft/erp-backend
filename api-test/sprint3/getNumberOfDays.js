var request = require('supertest');
var async = require('async');
var chai = require('chai');
var expect = chai.expect;

describe('GET /Number of days from fromDate and toDate',function(){
	
	it('should get the numberOfDays from fromDate=2016-03-24&&toDate=2016-03-28', function(done){
				request('http://localhost:5000/')
				.get('date?fromDate=2016-03-24&&toDate=2016-03-28')
				.expect(200, function(err, res){
					var leave = JSON.parse(res.text);
					expect(leave.numberOfDays).to.equal(2);
					done();
				});
	});
	it('should get the numberOfDays from fromDate=2016-03-24&&toDate=2016-03-29', function(done){
				request('http://localhost:5000/')
				.get('date?fromDate=2016-03-24&&toDate=2016-03-29')
				.expect(200, function(err, res){
					var leave = JSON.parse(res.text);
					expect(leave.numberOfDays).to.equal(3);
					done();
				});
	});
	it('should get the numberOfDays from fromDate=2016-03-26&&toDate=2016-03-27', function(done){
				request('http://localhost:5000/')
				.get('date?fromDate=2016-03-26&&toDate=2016-03-27')
				.expect(200, function(err, res){
					var leave = JSON.parse(res.text);
					expect(leave.numberOfDays).to.equal(0);
					done();
				});
	});
	it('should get the numberOfDays from fromDate=2016-03-24&&toDate=2016-03-26', function(done){
				request('http://localhost:5000/')
				.get('date?fromDate=2016-03-24&&toDate=2016-03-26')
				.expect(200, function(err, res){
					var leave = JSON.parse(res.text);
					expect(leave.numberOfDays).to.equal(1);
					done();
				});
	});
	it('should get the numberOfDays from fromDate=2016-03-24&&toDate=2016-03-24', function(done){
				request('http://localhost:5000/')
				.get('date?fromDate=2016-03-24&&toDate=2016-03-24')
				.expect(200, function(err, res){
					var leave = JSON.parse(res.text);
					expect(leave.numberOfDays).to.equal(1);
					done();
				});
	});


});
