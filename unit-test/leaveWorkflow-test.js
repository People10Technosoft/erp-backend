var mockery = require('mockery');
var sinon = require('sinon');
var chai = require('chai');
var expect = chai.expect;
                                            //istanbul cover node_modules/mocha/bin/_mocha unit-test/  --recursive
describe('My Test1', function(){

	beforeEach(function(){
    	mockery.enable({
    		useCleanCache: true,
    		warnOnReplace: false,
    		warnOnUnregistered: false
    	});
  });

  afterEach(function(){
    	mockery.disable();
  });


  it('should call 1st afterRemote method', function(done){
    var leaveWorkflow = require('../common/models/leave-workflow.js');
    var LeaveWorkflow = {};
    var utilMock = sinon.stub();
    var nextSpy = sinon.stub();
    LeaveWorkflow.afterRemote = sinon.stub();
    LeaveWorkflow.afterRemote.onCall(0).callsArgWith(1, {}, {}, nextSpy);
    LeaveWorkflow.remoteMethod = sinon.stub();
    LeaveWorkflow.observe = sinon.stub();
   // mockery.registerMock('../utilities.js', utilMock);
    leaveWorkflow(LeaveWorkflow);
    expect(nextSpy.called).to.be.true;
    done();
  });



  it('should fail the getLeaveWorkflows method in ErpKeysCode function', function(done){
    var LeaveWorkflow = {};
    var utilMock = {
      ErpKeysCode: sinon.stub()
    };
    LeaveWorkflow.afterRemote = sinon.stub();
    LeaveWorkflow.remoteMethod = sinon.stub();
    LeaveWorkflow.observe = sinon.stub();
    mockery.registerMock('../utilities.js', utilMock);
    utilMock.ErpKeysCode.onCall(0).callsArgWith(2, {
      error: 'error in setting the action taken keyCode'
    });
    var leaveWorkflow = require('../common/models/leave-workflow.js');
    leaveWorkflow(LeaveWorkflow);

    LeaveWorkflow.getLeaveWorkflows(1, function(err, leaveWorkflow){
      expect(utilMock.ErpKeysCode.called).to.be.true;
      expect(err).to.be.a('object');
      expect(err).to.eql({error: 'error in setting the action taken keyCode'});
      done();
    });
  });




  it('should fail the getLeaveWorkflows method in getWorkflow function', function(done){
    var LeaveWorkflow = {};
    var utilMock = {
      ErpKeysCode: sinon.stub()
    };
    var actionTakenLeaves = [];
    LeaveWorkflow.afterRemote = sinon.stub();
    LeaveWorkflow.remoteMethod = sinon.stub();
    LeaveWorkflow.observe = sinon.stub();
    mockery.registerMock('../utilities.js', utilMock);
    utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, actionTakenLeaves);
    LeaveWorkflow.find = sinon.stub();
    LeaveWorkflow.find.onCall(0).callsArgWith(1, {
      error: 'error in leaveWorkflow'
    });
    var leaveWorkflow = require('../common/models/leave-workflow.js');
    leaveWorkflow(LeaveWorkflow);

    LeaveWorkflow.getLeaveWorkflows(1, function(err, leaveWorkflow){
      expect(utilMock.ErpKeysCode.called).to.be.true;
      expect(LeaveWorkflow.find.called).to.be.true;
      expect(LeaveWorkflow.find.getCall(0).args[0].where).to.eql({leaveRequestId: 1});
      expect(err).to.be.a('object');
      expect(err).to.eql({error: 'error in leaveWorkflow'});

      done();
    });
  });



  it('should get leaveWorkflows', function(done){
    var LeaveWorkflow = {};
    var utilMock = {
      ErpKeysCode: sinon.stub()
    };
    var actionTakenLeaves = [
      {id: 1, type: 'actionTaken', keyCode: '1', keyValue: 'Apply'},
      {id: 2, type: 'actionTaken', keyCode: '2', keyValue: 'Approve'},
      {id: 3, type: 'actionTaken', keyCode: '3', keyValue: 'Deny'},
      {id: 4, type: 'actionTaken', keyCode: '4', keyValue: 'Sendback'},
    ];
    var leaveWorkflows = [
      {id: 10, actionTaken: 1, actionTimestamp: '2016-05-17 20:18:59',comments: 'birthday party',
        leaveRequestId: 1, toEmployee:2 ,fromEmployee: 3, 
        employee: function(){ 
          return {name : 'adarsh',
                  reportingManagerId:2
                  }
           
        }
      }
    ];

    var output = [
        {
          "actionTaken": 1,
          "actionTimestamp": "2016-05-17 20:18:59",
          "comments": "birthday party",
          "fromEmployee": "adarsh",
          "toEmployee": 2
        }
      ];

    LeaveWorkflow.afterRemote = sinon.stub();
    LeaveWorkflow.remoteMethod = sinon.stub();
    LeaveWorkflow.observe = sinon.stub();
    mockery.registerMock('../utilities.js', utilMock);
    utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, actionTakenLeaves);
    LeaveWorkflow.find = sinon.stub();
    LeaveWorkflow.find.onCall(0).callsArgWith(1, null, leaveWorkflows);

    var leaveWorkflow = require('../common/models/leave-workflow.js');
    leaveWorkflow(LeaveWorkflow);
    LeaveWorkflow.getLeaveWorkflows(1, function(err, leaveWorkflow){
      expect(utilMock.ErpKeysCode.called).to.be.true;
      expect(LeaveWorkflow.find.called).to.be.true;
      expect(LeaveWorkflow.find.getCall(0).args[0].where).to.eql({leaveRequestId: 1});
      expect(leaveWorkflow).to.eql(output);
      done();
    });
  });



  it('should get leaveWorkflows', function(done){
    var LeaveWorkflow = {};
    var utilMock = {
      ErpKeysCode: sinon.stub()
    };
    var actionTakenLeaves = [
      {id: 1, type: 'actionTaken', keyCode: '1', keyValue: 'Apply'},
      {id: 2, type: 'actionTaken', keyCode: '2', keyValue: 'Approve'},
      {id: 3, type: 'actionTaken', keyCode: '3', keyValue: 'Deny'},
      {id: 4, type: 'actionTaken', keyCode: '4', keyValue: 'Sendback'},
    ];
    var leaveWorkflows = [
      {id: 10, actionTaken: '1', actionTimestamp: '2016-05-17 20:18:59',comments: 'birthday party',
        leaveRequestId: 1, toEmployee:2 ,fromEmployee: 3, 
        employee: function(){ 
          return {name : 'adarsh',
                  reportingManagerId: null
                  }
           
        }
      }
    ];

    var output = [
        {
          "actionTaken": "Apply",
          "actionTimestamp": "2016-05-17 20:18:59",
          "comments": "birthday party",
          "fromEmployee": "adarsh",
          "toEmployee": 2,
          "lastApprover": 1
        }
      ];

    LeaveWorkflow.afterRemote = sinon.stub();
    LeaveWorkflow.remoteMethod = sinon.stub();
    LeaveWorkflow.observe = sinon.stub();
    mockery.registerMock('../utilities.js', utilMock);
    utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, actionTakenLeaves);
    LeaveWorkflow.find = sinon.stub();
    LeaveWorkflow.find.onCall(0).callsArgWith(1, null, leaveWorkflows);

    var leaveWorkflow = require('../common/models/leave-workflow.js');
    leaveWorkflow(LeaveWorkflow);
    LeaveWorkflow.getLeaveWorkflows(1, function(err, leaveWorkflow){
      expect(utilMock.ErpKeysCode.called).to.be.true;
      expect(LeaveWorkflow.find.called).to.be.true;
      expect(LeaveWorkflow.find.getCall(0).args[0].where).to.eql({leaveRequestId: 1});
      expect(leaveWorkflow).to.eql(output);
      done();
    });
  });


  it('should get leaveWorkflows', function(done){
    var LeaveWorkflow = {};
    var utilMock = {
      ErpKeysCode: sinon.stub()
    };
    var actionTakenLeaves = [
      {id: 1, type: 'actionTaken', keyCode: '1', keyValue: 'Apply'},
      {id: 2, type: 'actionTaken', keyCode: '2', keyValue: 'Approve'},
      {id: 3, type: 'actionTaken', keyCode: '3', keyValue: 'Deny'},
      {id: 4, type: 'actionTaken', keyCode: '4', keyValue: 'Sendback'},
    ];
    var leaveWorkflows = [
      {id: 10, actionTaken: '1', actionTimestamp: '2016-05-17 20:18:59',comments: 'birthday party',
        leaveRequestId: 1, toEmployee:2 ,fromEmployee: 3, 
        employee: function(){ 
          return {name : 'adarsh',
                  reportingManagerId: null
                  }
           
        }
      }
    ];

    var output = [
        {
          "actionTaken": "Apply",
          "actionTimestamp": "2016-05-17 20:18:59",
          "comments": "birthday party",
          "fromEmployee": "adarsh",
          "toEmployee": 2,
          "lastApprover": 1
        }
      ];

    LeaveWorkflow.afterRemote = sinon.stub();
    LeaveWorkflow.remoteMethod = sinon.stub();
    LeaveWorkflow.observe = sinon.stub();
    mockery.registerMock('../utilities.js', utilMock);
    utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, actionTakenLeaves);
    LeaveWorkflow.find = sinon.stub();
    LeaveWorkflow.find.onCall(0).callsArgWith(1, null, leaveWorkflows);

    var leaveWorkflow = require('../common/models/leave-workflow.js');
    leaveWorkflow(LeaveWorkflow);
    LeaveWorkflow.getLeaveWorkflows(1, function(err, leaveWorkflow){
      expect(utilMock.ErpKeysCode.called).to.be.true;
      expect(LeaveWorkflow.find.called).to.be.true;
      expect(LeaveWorkflow.find.getCall(0).args[0].where).to.eql({leaveRequestId: 1});
      expect(leaveWorkflow).to.eql(output);
      done();
    });
  });


  
  
  it('ErpKeysCode of utilities.js should fail LeaveWorkflow.observe', function(done){
    var LeaveWorkflow = {};
    var utilMock = {
      ErpKeysCode: sinon.stub()
    };
    var nextSpy = sinon.stub();
    var ctx = {
      instance: {
        actionByManager: 'Deny'
      }
    };
    LeaveWorkflow.afterRemote = sinon.stub();
    LeaveWorkflow.remoteMethod = sinon.stub();
    LeaveWorkflow.observe = sinon.stub();
    mockery.registerMock('../utilities.js', utilMock);
    LeaveWorkflow.observe.onCall(0).callsArgWith(1, ctx, nextSpy);
    utilMock.ErpKeysCode.onCall(0).callsArgWith(2, {
       error: 'error in setting leave status'
    });
    var leaveWorkflow = require('../common/models/leave-workflow.js');
    leaveWorkflow(LeaveWorkflow);
    expect(utilMock.ErpKeysCode.called).to.be.true;
    done();
    
  });

  it('should run but with status as Approve LeaveWorkflow.observe', function(done){
    var LeaveWorkflow = {};
    var utilMock = {
      ErpKeysCode: sinon.stub()
    };
    var nextSpy = sinon.stub();
    var ctx = {
      instance: {
        actionByManager: 'Approve',
        lastApprover: 1
      }
    };
    LeaveWorkflow.afterRemote = sinon.stub();
    LeaveWorkflow.remoteMethod = sinon.stub();
    LeaveWorkflow.observe = sinon.stub();
    mockery.registerMock('../utilities.js', utilMock);
    LeaveWorkflow.observe.onCall(0).callsArgWith(1, ctx, nextSpy);
    utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, {keyCode: 2});
    var leaveWorkflow = require('../common/models/leave-workflow.js');
    leaveWorkflow(LeaveWorkflow);
    expect(utilMock.ErpKeysCode.called).to.be.true;
    done();
    
  });


  it('LeaveRequest.updateAll should fail LeaveWorkflow.observe', function(done){
    var LeaveWorkflow = {};
    var utilMock = {
      ErpKeysCode: sinon.stub()
    };
    var nextSpy = sinon.stub();
    var ctx = {
      instance: {
        actionByManager: 'Approve',
        lastApprover: 1
      }
    };
    app = {
      models: {
        LeaveRequest: {
          updateAll: sinon.stub()
        }
      }
    };
    LeaveWorkflow.afterRemote = sinon.stub();
    LeaveWorkflow.remoteMethod = sinon.stub();
    LeaveWorkflow.observe = sinon.stub();
    mockery.registerMock('../utilities.js', utilMock);
    LeaveWorkflow.observe.onCall(0).callsArgWith(1, ctx, nextSpy);
    utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, {keyCode: 2});
    app.models.LeaveRequest.updateAll.onCall(0).callsArgWith(2, {
      error: 'update failed'
    },{});
    var leaveWorkflow = require('../common/models/leave-workflow.js');
    leaveWorkflow(LeaveWorkflow);
    expect(utilMock.ErpKeysCode.called).to.be.true;
    done();
    
  });


  it('should pass LeaveRequest.updateAll in LeaveWorkflow.observe', function(done){
    var LeaveWorkflow = {};
    var utilMock = {
      ErpKeysCode: sinon.stub()
    };
    var nextSpy = sinon.stub();
    var ctx = {
      instance: {
        actionByManager: 'Approve',
        lastApprover: 1
      }
    };
    app = {
      models: {
        LeaveRequest: {
          updateAll: sinon.stub()
        }
      }
    };
    LeaveWorkflow.afterRemote = sinon.stub();
    LeaveWorkflow.remoteMethod = sinon.stub();
    LeaveWorkflow.observe = sinon.stub();
    mockery.registerMock('../utilities.js', utilMock);
    LeaveWorkflow.observe.onCall(0).callsArgWith(1, ctx, nextSpy);
    utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, {keyCode: 2});
    app.models.LeaveRequest.updateAll.onCall(0).callsArgWith(2, null, {});
    var leaveWorkflow = require('../common/models/leave-workflow.js');
    leaveWorkflow(LeaveWorkflow);
    expect(utilMock.ErpKeysCode.called).to.be.true;
    done();
    
  });


  it('EmployeeSpecialLeaveEligibility.updateAll should fail LeaveWorkflow.observe', function(done){
    var LeaveWorkflow = {};
    var utilMock = {
      ErpKeysCode: sinon.stub()
    };
    var nextSpy = sinon.stub();
    var ctx = {
      instance: {
        actionByManager: 'Deny',
        lastApprover: 1
      }
    };
    app = {
      models: {
        LeaveRequest: {
          updateAll: sinon.stub()
        },
        EmployeeSpecialLeaveEligibility: {
          updateAll: sinon.stub()
        }
      }
    };

    LeaveWorkflow.afterRemote = sinon.stub();
    LeaveWorkflow.remoteMethod = sinon.stub();
    LeaveWorkflow.observe = sinon.stub();
    mockery.registerMock('../utilities.js', utilMock);
    LeaveWorkflow.observe.onCall(0).callsArgWith(1, ctx, nextSpy);
    utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, {keyCode: 2});
    app.models.LeaveRequest.updateAll.onCall(0).callsArgWith(2, null, {});
    app.models.EmployeeSpecialLeaveEligibility.updateAll.onCall(0).callsArgWith(2, {
      error: 'update failed'
    },{});
    var leaveWorkflow = require('../common/models/leave-workflow.js');
    leaveWorkflow(LeaveWorkflow);
    expect(utilMock.ErpKeysCode.called).to.be.true;
    done();
    
  });




  it('EmployeeSpecialLeaveEligibility.updateAll should pass LeaveWorkflow.observe', function(done){
    var LeaveWorkflow = {};
    var utilMock = {
      ErpKeysCode: sinon.stub()
    };
    var nextSpy = sinon.stub();
    var ctx = {
      instance: {
        actionByManager: 'Deny',
        lastApprover: 1
      }
    };
    app = {
      models: {
        LeaveRequest: {
          updateAll: sinon.stub()
        },
        EmployeeSpecialLeaveEligibility: {
          updateAll: sinon.stub()
        }
      }
    };

    LeaveWorkflow.afterRemote = sinon.stub();
    LeaveWorkflow.remoteMethod = sinon.stub();
    LeaveWorkflow.observe = sinon.stub();
    mockery.registerMock('../utilities.js', utilMock);
    LeaveWorkflow.observe.onCall(0).callsArgWith(1, ctx, nextSpy);
    utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, {keyCode: 2});
    app.models.LeaveRequest.updateAll.onCall(0).callsArgWith(2, null, {});
    app.models.EmployeeSpecialLeaveEligibility.updateAll.onCall(0).callsArgWith(2, null, {});
    var leaveWorkflow = require('../common/models/leave-workflow.js');
    leaveWorkflow(LeaveWorkflow);
    expect(utilMock.ErpKeysCode.called).to.be.true;
    done();
    
  });



it('should pass LeaveWorkflow.observe', function(done){
    var LeaveWorkflow = {};
    var utilMock = {
      ErpKeysCode: sinon.stub()
    };
    var nextSpy = sinon.stub();
    var ctx = {
      instance: {
        actionByManager: 'Sendback',
        lastApprover: 1
      }
    };

    LeaveWorkflow.afterRemote = sinon.stub();
    LeaveWorkflow.remoteMethod = sinon.stub();
    LeaveWorkflow.observe = sinon.stub();
    mockery.registerMock('../utilities.js', utilMock);
    LeaveWorkflow.observe.onCall(0).callsArgWith(1, ctx, nextSpy);
    var leaveWorkflow = require('../common/models/leave-workflow.js');
    leaveWorkflow(LeaveWorkflow);
    expect(nextSpy.called).to.be.true;
    done();
    
  });



});
