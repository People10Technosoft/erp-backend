module.exports = function(LeaveRequest) {
	var specialLeaves = require('../leave-options.js');
	var leaveSummary = require('../leave-statistics.js');
	var leaveRequestList = require('../getLeaveRequestList.js');
	var util = require('../utilities.js');
	var async = require('async');
	var ejs = require('ejs');
	var fs = require('fs');
	var moment = require('moment');
	require('moment-weekday-calc');


	LeaveRequest.afterRemote('**',function(context, user, next){		
	console.log("-----------------------------",context.methodString);
	next();
	});


	LeaveRequest.getList =function(id, clb){											//Function to get all the leave request of a particular employee for the current year
		var leaveData = [];
		var pendingLeaves = {};
		var pendingLeavesData = [];
		var takenLeaves = {};
		var takenLeavesData = [];
		var toBeTakenLeaves = {};
		var toBeTakenLeavesData = [];
		var currentDate = new Date();

		async.waterfall([
		function getLeaveList(cb){														//Function to get the list of all leave requests
			leaveRequestList.getList(id, function(err, leaves){
				if(err){
					return cb(err);
				}
				if(leaves.length < 1){
					return cb(null);
				}
				async.forEachOf(leaves,function(leave, index, callback){				//to loop through all leaves to get the leaves under pending,future and raken category if any
					if(leave.status === "Pending"){
						pendingLeavesData.push(leave);
						callback(null);
					}	
					else if(leave.status === "Approved"){
						if(moment(currentDate).isBefore(leave.fromDate)){
							toBeTakenLeavesData.push(leave);
						}
						else{
							takenLeavesData.push(leave);
						}
					callback(null);
					}
					else{
						callback(null);
					} 
				}
				,function(err){
					if(err){
						return cb(err);
					}
					else{
						cb(null);
					}
				});
			});
		},

		function leaveFormatting(callback){													//data to be send
			if(pendingLeavesData.length > 0){
				pendingLeaves.title = "Pending";
				pendingLeaves.data = pendingLeavesData;
				leaveData.push(pendingLeaves);
			}
			if(toBeTakenLeavesData.length > 0){
				toBeTakenLeaves.title = "Future";
				toBeTakenLeaves.data = toBeTakenLeavesData;
				leaveData.push(toBeTakenLeaves);
			}
			if(takenLeavesData.length > 0){
				takenLeaves.title = "Taken";
				takenLeaves.data = takenLeavesData;
				leaveData.push(takenLeaves);
			}
			callback(null);
		}

		]
		,function(err, response){															//function to send the result to calling function
			if(err){
				return clb(err);
			}
			else{
				clb(null, leaveData);
			}
		});															
		
	};


	LeaveRequest.remoteMethod('getList',													//Registering the getList Method in the list of Api's
	 	{
	 		http :{path:'/getList', verb: 'get'},
	 		accepts:{arg:'id', type: 'string', required: true},
	 		returns: {type: 'string', root:true}
	
	 	}
	);
	 


	LeaveRequest.leaveStatistics = function(id, cb){     			           				//API to get the leave statistics
      	leaveSummary.leaveStatistics(id, function(err, response){
			if(err){
				return cb(err);
			}
			else{
				cb(null, response);
			}
		});
	};


	LeaveRequest.remoteMethod('leaveStatistics',											//Registering the leave statistics Method in the list of Api's
	 	{
	 		http :{path:'/leaveStatistics', verb: 'get'},
	 		accepts:{arg:'id', type: 'string', required: true},
	 		returns: {type: 'string', root: true }
	 	}
	 	);


	/*
		Data to be send to cancel a leave request
		{
         	"action":"cancel",
         	"type":type,
         	"id":leaveID,
         	"fromDate": fDate,
         	"toDate":tDate
       };
	*/

	/*
		Data to be send to update a leave request
		{
  			"fromDate": "2016-07-20",
  			"toDate": "2016-07-20",
  			"numberOfDays": 1,
  			"employeeId": "P1012349",
  			"halfDaySlot":0,
  			"approverId": "P1012348",
  			"comments": "after sendback",
  			"action":"update",
  			"id":358,
  			"type":"Privilege"
		}

	*/


	LeaveRequest.beforeRemote('prototype.updateAttributes',function(context, user, next){   //to update the status of cancelled request
		var req = context.req.body;
		var typeOfLeaveRequest = req.type;

		if(req.action === "cancel"){														//if the request action is cancel

			async.waterfall([
			function updateLeaveStatusInLeaveRequest(callback){								//function to get the leave status of cancelled leave
				util.ErpKeysCode("leave_status", "Cancelled", function(err, key){
					if(err){
						return callback(err);
					}
					else{
						req.status = key.keyCode;
						callback(null);
					}
				});
			},

			function updateSpecialLeaves(callback){											//function to update the special leaves
				if((typeOfLeaveRequest === "Paternity")||(typeOfLeaveRequest === "Maternity")||(typeOfLeaveRequest === "Comp-off")){
					app.models.EmployeeSpecialLeaveEligibility.updateAll(					//update query
					{leaveRequestId:req.id}, {status: 0, leaveRequestId: null}, function(err, response){
						if(err){
							return callback(err);
						}
						else{
							callback(null);
						}
					});
				}
				else{
					callback(null);
				}
			},

			]
			,function(err, response){														//function returning the result
				if(err){
					return next(err);
				}
				else{
					next();
				}
			});
		}
		else if(req.action === "update")													//if the request action is update
		{
			var specialLeaveId = null;
			if(req.numberOfDays === 0){
				return next("IT'S ALREADY A HOLIDAY");
			}
			var fromDate = new Date(req.fromDate);
			var day = fromDate.getDay();
			if(day === 0 || day === 6){
				return next("IT'S ALREADY A HOLIDAY:Please Pay Attention");
			}
			async.waterfall([
			function updateSpecialLeaveStatusForSpecialLeaves(callback){					//function to update special type of leaves of the corresponding leave to its initial state
				var typeOfLeaveRequest = req.type;
				if((typeOfLeaveRequest === "Paternity")||(typeOfLeaveRequest === "Maternity")||(typeOfLeaveRequest === "Comp-off")){
					app.models.EmployeeSpecialLeaveEligibility.updateAll(			//query to update the employeeSpecialLeaveTable to its initial state
					{leaveRequestId:req.id}, {status: 0, leaveRequestId: null}, function(err, response){
						if(err){		
							return callback(err);
						}
						else{
						specialLeaveId = response.id;
						callback(null);
						}
					});
				}
				else{
					callback(null);
				}
			},

			function dateConflicts(callback){												//function to check for the date conflicts										
				util.dateConflicts(req,function(err, response){								//calling the dateConflicts function from utility.js file
					if(err){
						return callback(err);
					}
					else{
						callback(null);
					}
				});
			},

			function settingDataToBeUpdated(callback){										//function for data formatting to update the leave request
				var currentDate = new Date().toLocaleString();
				util.ErpKeysCode("action_taken", "Apply",function(err, key){				//calling ErpKeysCode function to get the action taken code for apply 
					if(err){
						return callback(err);
					}
					var data = {
						"fromEmployee": req.employeeId,
						"toEmployee": req.approverId,
						"actionTaken": key.keyCode,
						"actionTimestamp": currentDate,
						"comments": req.comments,
						"leaveRequestId": req.id
					};
				
					callback(null, data);
				});
			},

			function leaveWorkflowEntry(data, callback){									//update the leave workflow table according to the update
				app.models.LeaveWorkflow.create(data ,function(err, obj){
					if(err){
						return callback(err);
					}
					else{
						callback(null);
					}
				});
			},

			function updateSpecialLeaves(callback){											//update special leave table for the corresponding employee
				util.updateSpecialLeaves(req, function(err,response){						//calling the updateSpecialLeave function from the utilities.js
					if(err){
						return callback(err);
					}
					else{
						callback(null);
					}
				});
			}
			]
			,function(err, response){														//function returning from beforeRemote hook
				if(err){
					return next(err);
				}
				else{
					next();
				}
			});
		}

	});	


	LeaveRequest.afterRemote('prototype.updateAttributes',function(context, output, next){ 
		var res = context.result;
		
		async.waterfall([
		function getEmployeeDetails(callback){												//function to get the all employees details
			app.models.Employee.findOne({
			where: {id: res.employeeId},
			fields: {reportingManagerId: true ,name: true, empId: true, designation: true, email: true}}, function(err, employeeData){
				if(err){
					return callback(err);
				}
				else{
					callback(null, employeeData);
				}
			});					
		},

		function getManagerDetails(employeeData, callback){									//function to get the manager details
			app.models.Employee.findOne({
			where: {id: employeeData.reportingManagerId},
			fields: {reportingManagerId: true ,name: true, empId: true, designation: true, email: true}}, function(err, managerData){
				if(err){
					return callback(err);
				}
				else{
					callback(null, employeeData, managerData);
				}
			});					
		},

		function sendEmailNotification(employeeData, managerData, callback){				//function to send the email notification to the manager
			var emailData = {
					actionByEmployee: res.action,
					employeeEmpId: employeeData.empId,
					employeeName: employeeData.name,
					managerEmail: managerData.email,
					employeeEmail: employeeData.email,
					employeeDesignation: employeeData.designation,
					comments: res.comments,
					leaveFromDate: moment(res.fromDate).format("dddd, MMMM Do YYYY"),
					leaveToDate: moment(res.toDate).format("dddd, MMMM Do YYYY"),
					numberOfDays: res.numberOfDays
			};
			util.sendEmailNotificationToManager(emailData, function(err, response){ 		//calling the sendEmailNotificationToManager from utilities.js
				if(err){
					return callback("Email Notification Failed");
				}
			});
			callback(null);
		}
		]
		,function(err){																		//function returning from afterRemote hook
			if(err){
				return next(err);
			}
			else{
				next();
			}
		});
	});


	/*
		Data needs to be send(from UI)
		{
  			"fromDate": "2016-03-23",                                
  			"toDate": "2016-03-23",
  			"numberOfDays": 1,
 			"description": "sick leave",
  			"type": "Loss of Pay",
 			"employeeId": "3",
  			"halfDaySlot":0,
  			"approverId": "2"
		}
		Other options for type field://Privilege,COMP OFF,Paternity,Maternity
		Other option for halfDaySlot field:// or 0
	*/



	LeaveRequest.beforeRemote('create',function(context, user, next){						//to apply a leave request
		var req = context.req;
		console.log("request ",req.body);
		if(req.body.approverId === undefined || req.body.approverId === null){
			console.log("NO APPROVER");
			return next("There is no approver for this employee");
		}
		var pendingStatusCode ;
		var cancelStatus;
		var count_date_match = 0;
		var count_privilege = 0;
		var typeOfLeaveRequest = req.body.type;
		var fromDate = new Date(req.body.fromDate);
		var day = fromDate.getDay();
		if(day === 0 || day === 6){
			return next("IT'S ALREADY A HOLIDAY:Please Pay Attention");
		}
		req.body.leaveRequestYear = fromDate.getFullYear();
		if(req.body.numberOfDays === 0){
			return next("IT'S ALREADY A HOLIDAY");
		}
		async.waterfall([
		function pendingStatusCode(callback){												//function to get the pendingStatus code 
			util.ErpKeysCode("leave_status", "Pending", function(err, key){
				if(err){
					return callback(err);
				}
				else{
					req.body.status = key.keyCode;
					//console.log("1st phase",req.body);
					callback(null);
				}
			});
		},

		function dateConflicts(callback){													//function to check for date conflicts
			util.dateConflicts(req.body, function(err, response){							//function to call the dateConflicts method from tthe utilities.js
				if(err){
					return callback(err);
				}
				else{
					callback(null);
				}
			});
		}
		]
		,function(err, response){															//function returning from the beforeRemote hook
			if(err){
			  return next(err);
			}
			else{
				next();
			}
		});
	});		



	LeaveRequest.afterRemote('create',function(context, output, next){						//after creating the leave request
		var res = context.result;
		console.log("response in afterRemote",res);
		var currentDate = new Date().toLocaleString();
		var typeOfLeaveRequest = res.type;

		async.waterfall([
		function updateSpecialLeaves(callback){												//update the special leaves table
			util.updateSpecialLeaves(res, function(err,response){
				if(err){
					return callback(err);
				}
				else{
					callback(null);
				}
			});
		},

		function dataFormattingForLeaveWorkflowEntry(callback){								//function for data formatting for the leave workflow entry
			util.ErpKeysCode("action_taken", "Apply", function(err, key){					//calling function of ErpKeysCOde of utilities.js to find the actionTaken code for leave apply 
				if(err){
					return callback(err);
				}
				var data = {
					"fromEmployee": res.employeeId,
					"toEmployee": res.approverId,
					"actionTaken": key.keyCode,
					"actionTimestamp": currentDate,
					"comments": res.description,
					"leaveRequestId": res.id
				};
				callback(null, data);
			});
		},

		function updateLeaveWorkflowTable(data, callback){									//function to update the leaveWorkflow table
			app.models.LeaveWorkflow.create(data , function(err, obj){
				if(err){
					return callback(err);
				}
				else{
					callback(null);
				}
			});
		},

		function getEmployeeDetails(callback){												//function to get the employee details
			app.models.Employee.findOne({
			where: {id: res.employeeId},
			fields: {reportingManagerId: true ,name: true, empId: true, designation: true, email: true}}, function(err, employeeData){
				if(err){
					return callback(err);
				}
				else{
					callback(null, employeeData);
				}
			});					
		},

		function getManagerDetails(employeeData, callback){									//function to get the manager details
			app.models.Employee.findOne({													//query to find the employee
			where: {id: res.approverId},
			fields: {reportingManagerId: true ,name: true, empId: true, designation: true, email: true}}, function(err, managerData){
				if(err){
					return callback(err);
				}
				else{
					callback(null, employeeData, managerData);
				}
			});					
		},

		function sendEmailNotification(employeeData, managerData, callback){				//function to send email notification to the manager regarding leave application
			var emailData = {
					actionByEmployee : "Applied",
					managerId : managerData.empId,
					employeeEmpId : employeeData.empId,
					employeeName : employeeData.name,
					managerEmail : managerData.email,
					employeeEmail : employeeData.email,
					employeeDesignation : employeeData.designation,
					description : res.description ,
					leaveFromDate : moment(res.fromDate).format("dddd, MMMM Do YYYY"),
					leaveToDate : moment(res.toDate).format("dddd, MMMM Do YYYY"),
					numberOfDays : res.numberOfDays
			};
			util.leaveApplicationNotification(emailData, function(err, response){
				if(err){
					return callback("Email Notification Failed");
				}
			});
			callback(null);
		}
		]
		,function(err, response){															//function returning from afterRemote hook
			if(err){
				return next(err);
			}
			else{
				next();
			}
		});
	});




	

	LeaveRequest.leaveRequestsWaitingForApproval = function(id, cb){						//function to get the leave requests waiting for approval from a manager
		var data =[];
		var cancelStatus;
		var currentDate = new Date();

		async.waterfall([
		function cancelStatusValue(callback){												//function to get the cancel leave status code
			util.ErpKeysCode("leave_status", "Cancelled", function(err, res){
				if(err){
					return callback(err);
				}
				else{
					cancelStatus = res.keyCode;
					callback(null);
				}
			});
		},

		function getLatestTimestamps(callback){												//function to get the latest entry from the leave workflow corresponding to the particular leave request
			util.latestTimestamp(id, function(err, leaveTimestamps){
				if(err){
					return cb(err);
				}
				else{
					callback(null, leaveTimestamps);	
				}
			});
		},

		function findLeaveWorkflows(leaveTimestamps, clb){									//function to get the leave workflows 
			var leaveRequest = {};
				//console.log("leaveRequestsWaitingForApproval is called", leaveTimestamps);
			async.eachSeries(leaveTimestamps, function(leaveTimestamp, callback){
				var leaveId = leaveTimestamp.leave_request_id;
				var actionTimestamp = 	new Date(leaveTimestamp.action_timestamp).toLocaleString();
				console.log("&&&&&&&&&&&&&&&&",leaveId, actionTimestamp);
				app.models.LeaveWorkflow.findOne({											//query to find the leave workflow correspondind to the leave id 
				where: {and: [{toEmployee: id}, {leaveRequestId: leaveId}, {actionTimestamp: actionTimestamp}]}
				,include: {
					relation: 'leaveRequest',
					scope:{
						include: {
							relation: 'employee',
							scope: {
								fields: ['name', 'empId', 'designation']
							}
						}
					}
				}
				},function(err, leaveWorkflow){
					if(err){
						return callback(err);
					}
					console.log("leaveRequestsWaitingForApproval is called", leaveWorkflow, id);
					if(leaveWorkflow !== null && leaveWorkflow.leaveRequest().employeeId!==id && leaveWorkflow.leaveRequest().status!== cancelStatus){ 
						var leaveRequestData = leaveWorkflow.leaveRequest();
						leaveRequest = {};
						console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%", leaveWorkflow, id);
						leaveRequest.fromEmployee = leaveRequestData.employeeId;
						leaveRequest.employeeOfficialId = leaveRequestData.employee().empId;
						leaveRequest.fromDate = leaveRequestData.fromDate;
						leaveRequest.toDate = leaveRequestData.toDate;
						leaveRequest.numberOfDays = leaveRequestData.numberOfDays;
						leaveRequest.typeOfLeave = leaveRequestData.typeOfLeavesId;
						leaveRequest.description = leaveRequestData.description;
						leaveRequest.employeeName = leaveRequestData.employee().name;
						leaveRequest.employeeFirstName = leaveRequestData.employee().firstName;
						leaveRequest.employeeLastName = leaveRequestData.employee().lastName;
						leaveRequest.leaveId = leaveRequestData.id;

						if(leaveRequestData.halfDaySlot !== 0){
							leaveRequest.halfDay = leaveRequestData.halfDaySlot;
						}
						app.models.ErpKeys.findOne({where: {and: [{type: "designation"}, {keyCode: leaveRequestData.employee().designation}]}}, function(err, key){
							if(err){
								return callback(err);
							}
							else{
								leaveRequest.designation = key.keyValue;
								data.push(leaveRequest);	
								callback(null);	
							}
						});
					}
					else{
						callback(null);
					}
				});
			}, function(err){																//function returning from async.eachSeries
				if(err){
					return clb(err);
				}
				else{
					clb(null);
				}
			});
		},

		function leaveTypeFormatting(cb){													//function to format the leave type
			async.each(data, function(leaveData, callback){									//function to find the type of leave request corresponding to the leave type recieved from UI
				app.models.TypeOfLeaves.findOne({
				where: {id: leaveData.typeOfLeave}}
				,function(err, type){
					if(err){
						return callback(err);
					}
					leaveData.leaveType = type.leaveType;
					leaveSummary.leaveStatistics(leaveData.fromEmployee, function(err, statistics){
						if(err){
							return callback(err);
						}
						leaveData.leaveRemaining = statistics.remaining;
						leaveData.totalLeavesTaken = statistics.availed;
						callback(null);	
					});	
				});
			}
			,function(err){																	//function returning from asyn.each
				if(err){
					return cb(err);
				}
				else{
					cb(null);
				}
			});
		},


		function findAppliedOn(cb){													
			async.each(data, function(leaveData, callback){			
			console.log("######", leaveData);						
				app.models.LeaveWorkflow.findOne({										
						where: {and: [{leaveRequestId: leaveData.leaveId}, {fromEmployee: leaveData.fromEmployee}]},
						order: 'actionTimestamp ASC'
						}, function(err, timestamp){
							if(err){
								return callback(err);
							}
							else{
								console.log("######", timestamp);
								leaveData.appliedOn = timestamp.actionTimestamp;
								callback(null);
							}
						});
			}
			,function(err){																	//function returning from asyn.each
				if(err){
					return cb(err);
				}
				else{
					cb(null);
				}
			});
		},

		function appovedStatusCode(callback){												//function to find the "approved" leave status code 
			util.ErpKeysCode("leave_status", "Approved", function(err, key){
				if(err){
					return callback(err);
				}
				else{
					var approvedStatus = key.keyCode;
					console.log("1st phase",approvedStatus);
					callback(null, approvedStatus);
				}
			});
		},

		function lastLeaveTakenData(approvedStatus, cb){									//function to get the last leave taken details
			async.each(data, function(leaveData, callback){									//to loop through all the approved leave requests to find the last taken leave
				LeaveRequest.find({
				where: {and: [{employeeId:leaveData.fromEmployee},{status: approvedStatus},{fromDate: {lte: currentDate}}]},
				order: 'id DESC',
				limit: 1
				}, function(err, leaves){
					if(err){
						return callback(err);
					}
					if(leaves.length > 0){
						console.log("approvedStatus",approvedStatus);
						leaveData.lastLeaveTakenFromDate = leaves[0].fromDate;
						leaveData.lastLeaveTakenToDate = leaves[0].toDate;
						callback(null);
					}
					else{
						callback(null);
					}
				});
			}
			,function(err){																	//function returning from async.each
				if(err){
					cb(err);
				}
				else{
					cb(null);
				}
			});
		}
		],function(err, response){															//function returning the result of the API
			if(err){
				return cb(err);
			}
			else{
				cb(null, data);
			}
		});
	};


	LeaveRequest.remoteMethod('leaveRequestsWaitingForApproval',							//Registering the getList Method in the list of Api's
	 	{
	 		http :{path:'/leaveRequestsWaitingForApproval', verb: 'get'},
	 		accepts:{arg:'id', type: 'string', required: true},
	 		returns: {type: 'string', root:true}
	 	}
	);


	
};
