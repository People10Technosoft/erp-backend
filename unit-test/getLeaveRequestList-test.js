var mockery = require('mockery');
var sinon = require('sinon');
var chai = require('chai');
var expect = chai.expect;
                                            //istanbul cover node_modules/mocha/bin/_mocha unit-test/  --recursive
describe('My Test1', function(){

	beforeEach(function(){
    	mockery.enable({
    		useCleanCache: true,
    		warnOnReplace: false,
    		warnOnUnregistered: false
    	});
  	});

  	afterEach(function(){
    	mockery.disable();
  	});


    it('should give an error at the first call of ErpKeysCode ', function(done){

  		var utilMock = {
  			ErpKeysCode : sinon.stub()
  		};
  		utilMock.ErpKeysCode.onCall(0).callsArgWith(2, {
      	error: 'Cancel status is undefined'
    	},
    	{
    	  keyCode : 3
    	});

  		mockery.registerMock('./utilities.js', utilMock);
  		var getLeaveRequestList = require('../common/getLeaveRequestList');

  		getLeaveRequestList.getList(2, function(err, list){
  			expect(utilMock.ErpKeysCode.called).to.be.true;
  			expect(err).to.be.a('object');
  			expect(err.error).to.equal('Cancel status is undefined');
  			done();
  		});
  	});


  	it('should give an error at the second call of ErpKeysCode ', function(done){

  		var utilMock = {
  			ErpKeysCode : sinon.stub()
  		};
  		utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null,
    	{
    	  keyCode : 4
    	});

  		utilMock.ErpKeysCode.onCall(1).callsArgWith(2, {
      	error: 'Deny status is undefined'
    	},
    	{
    	  keyCode : 3
    	});

  		mockery.registerMock('./utilities.js', utilMock);
  		var getLeaveRequestList = require('../common/getLeaveRequestList');

  		getLeaveRequestList.getList(2, function(err, list){
  			expect(utilMock.ErpKeysCode.called).to.be.true;
  			expect(err).to.be.a('object');
  			expect(err.error).to.equal('Deny status is undefined');
  			expect(err).to.eql({error: 'Deny status is undefined'});
  			done();
  		});
  	});


  	it('should give an error at the first call of leaveRequests', function(done){

  		var utilMock = {
  			ErpKeysCode : sinon.stub()
  		};
  		utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null,
    	{
    	  keyCode : 4
    	});

  		utilMock.ErpKeysCode.onCall(1).callsArgWith(2, null,
    	{
    	  keyCode : 3
    	});
      app = {
        models: {
          LeaveRequest: {
            find: sinon.stub()
          }
        }
      };

      app.models.LeaveRequest.find.onCall(0).callsArgWith(1, {
        error: 'leave request error'
      });

      var expectedQuery = {
       where: {and: [{employeeId:125}, {leaveRequestYear:2016}, {status: {nin: [4, 3]}}]}
      };

  		mockery.registerMock('./utilities.js', utilMock);
  		var getLeaveRequestList = require('../common/getLeaveRequestList');
      getLeaveRequestList.getList(125, function(err, list){
        expect(app.models.LeaveRequest.find.called).to.be.true;
        expect(app.models.LeaveRequest.find.getCall(0).args[0].where).to.eql(expectedQuery.where);
        expect(err).to.eql({error: 'leave request error'});
        done();
      });
  	});



    it('should give an error at the third call of ErpKeysCode', function(done){

      var utilMock = {
        ErpKeysCode : sinon.stub()
      };
      utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null,
      {
        keyCode : 4
      });

      utilMock.ErpKeysCode.onCall(1).callsArgWith(2, null,
      {
        keyCode : 3
      });

      utilMock.ErpKeysCode.onCall(2).callsArgWith(2, {
        error: 'leave status is undefined'
      },
      {
        keys: []
      });

      app = {
        models: {
          LeaveRequest: {
            find: sinon.stub()
          }
        }
      };

      app.models.LeaveRequest.find.onCall(0).callsArgWith(1, null, []);


      mockery.registerMock('./utilities.js', utilMock);
      var getLeaveRequestList = require('../common/getLeaveRequestList');
      getLeaveRequestList.getList(125, function(err, list){
        expect(app.models.LeaveRequest.find.called).to.be.true;
        expect(utilMock.ErpKeysCode.called).to.be.true;
        expect(err).to.be.a('object');        
        expect(err).to.eql({ error: 'leave status is undefined'});
        done();
      });
    });


    it('should give an empty array as the result', function(done){

      var utilMock = {
        ErpKeysCode : sinon.stub()
      };
      utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null,
      {
        keyCode : 4
      });

      utilMock.ErpKeysCode.onCall(1).callsArgWith(2, null,
      {
        keyCode : 3
      });

      utilMock.ErpKeysCode.onCall(2).callsArgWith(2, null,
      {
        keys: []
      });

      app = {
        models: {
          LeaveRequest: {
            find: sinon.stub()
          }
        }
      };

      app.models.LeaveRequest.find.onCall(0).callsArgWith(1, null, []);


      mockery.registerMock('./utilities.js', utilMock);
      var getLeaveRequestList = require('../common/getLeaveRequestList');
      getLeaveRequestList.getList(125, function(err, list){
        expect(app.models.LeaveRequest.find.called).to.be.true;
        expect(utilMock.ErpKeysCode.called).to.be.true;
        expect(list).to.eql([]);
        done();
      });
    });


    it('should give proper result', function(done){

      var utilMock = {
        ErpKeysCode : sinon.stub()
      };
      utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null,
      {
        keyCode : 4
      });

      utilMock.ErpKeysCode.onCall(1).callsArgWith(2, null,
      {
        keyCode : 3
      });
      var keysValue = [
      {id: 5, type: 'leaveStatus', keyCode: 1, keyValue: 'Approved'},
      {id: 6, type: 'leaveStatus', keyCode: 2, keyValue: 'Pending'}
      ];
      utilMock.ErpKeysCode.onCall(2).callsArgWith(2, null, keysValue);

      app = {
        models: {
          LeaveRequest: {
            find: sinon.stub()
          }
        }
      };
      var leaveRequest = [
      {id: 221, leaveRequestYear: 2016, fromDate: '2016-06-01', toDate: '2016-06-01',
       numberOfDays: 1, status: 1, description: 'test', halfDaySlot: 0,
       typeOfLeaveId: 1, employeeId: 125, 
       typeOfLeaves : function(){
        return {
          leaveType : 'Privilege'
        }
      }
      }
      ]; 
      app.models.LeaveRequest.find.onCall(0).callsArgWith(1, null, leaveRequest);

      var finalData = [
          { 
            fromDate: '2016-06-01',
            toDate: '2016-06-01',
            numberOfdays: 1,
            status: 'Approved', 
            description: 'test', 
            halfDaySlot: 0,
            typeOfLeave: 'Privilege',
            leaveRequestId: 221, 
      }];
      mockery.registerMock('./utilities.js', utilMock);
      var getLeaveRequestList = require('../common/getLeaveRequestList');
      getLeaveRequestList.getList(125, function(err, list){
        expect(app.models.LeaveRequest.find.called).to.be.true;
        expect(utilMock.ErpKeysCode.called).to.be.true;
        expect(list.length).to.equal(1);
        expect(list).to.eql(finalData);
        done();
      });
    });



});


