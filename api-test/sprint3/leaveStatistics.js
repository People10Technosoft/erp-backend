var request = require('supertest');
var async = require('async');
var chai = require('chai');
var expect = chai.expect;

describe('GET /Leave Statistics',function(){
	beforeEach(function(){
		request = request('http://localhost:5000/api')
	});
	afterEach(function(){

	});

	it('Can apply a leave-request', function(done){
		async.waterfall([
			function getLeaveStatistics(callback){
				request
				.get('/leave_requests/leaveStatistics/')
				.send({id: 'P1012346'})
				.expect(200, function(err, res){
					var leaveStatistics = JSON.parse(res.text);
					callback(null,leaveStatistics);
				})
			},
			function applyNewLeaveRequest(leaveStatistics, callback){
				request
				.post('/leave_requests')
				.send({fromDate: '2016-03-28T00:00:00.000Z',
						toDate: '2016-03-29T00:00:00.000Z',
						numberOfDays: '2',
  						description: 'sick leave',
  						type: 'Privilege',
  						employeeId: 'P1012346',
  						halfDaySlot: '1',
  						approverId: 'P1012345'})
				.expect(200, function(err, res){
					var leave = JSON.parse(res.text);
					callback(null, leave.id, leave.employeeId, leaveStatistics);
				});	
			},
			function getNewlyAppliedLeaveRequest(id ,employeeId, leaveStatistics, callback){
				request
				.get('/leave_requests/'+ id)
				.expect(200, function(err, res){
					var leave = JSON.parse(res.text);
					expect(leave.id+"").to.equal(id);
					expect(leave.employeeId).to.equal(employeeId);
					expect(leave.fromDate).to.equal('2016-03-28T00:00:00.000Z');
					expect(leave.toDate).to.equal('2016-03-29T00:00:00.000Z');
					expect(leave.numberOfDays).to.equal(2);
					expect(leave.description).to.equal('sick leave');
					callback(null, id, leaveStatistics);
				})
			},
			function getLeaveStatistics(id , leaveStatistics, callback){
				request
				.get('/leave_requests/leaveStatistics/')
				.send({id: 'P1012346'})
				.expect(200, function(err, res){
					var leaveStats = JSON.parse(res.text);
					expect(leaveStats.pending).to.equal(leaveStatistics.pending+2);
					expect(leaveStats.LOP).to.equal(leaveStatistics.LOP);
					expect(leaveStats.privilege).to.equal(leaveStatistics.privilege+2);
					expect(leaveStats.remaining).to.equal(leaveStatistics.remaining-2);
					expect(leaveStats.totalApproved).to.equal(leaveStatistics.totalApproved);
					expect(leaveStats.totalLeaves).to.equal(leaveStatistics.totalLeaves);
					callback(null, id);
				})
			},
			function deleteLeaveRequest(id, callback){
				request
				.delete('/leave_requests/' + id)
				.expect(200, function(err, res){
					console.log(res.status);
					expect(res.status).to.equal(200);
					callback();
				});
			}
			],function(err){
				done();
			});
	})
});
