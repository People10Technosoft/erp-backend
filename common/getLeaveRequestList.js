var util = require('./utilities.js');
var async = require('async');

var getList = function(id, cb){																		//function to get the list of all leave request wrt an employee
	var currentDate = new Date();
	var currentYear = currentDate.getFullYear();
	var leaveData = [];
	var cancelStatus;
	var deniedStatus;

	async.waterfall([
		function cancelStatusValue(callback){														//function to get the status code for the cancelled leave
			util.ErpKeysCode("leave_status", "Cancelled" ,function(err, res){
				if(err){
					return callback(err);
				}
				else{
					cancelStatus = res.keyCode;
					callback(null);
				}
			});
		},

		function deniedStatusValue(callback){														//function to get the status code for the denied leave
			util.ErpKeysCode("leave_status", "Denied" ,function(err, res){
				if(err){
					callback(err);
				}
				else{
					deniedStatus = res.keyCode;
					callback(null);
				}
			});
		},

		function leaveRequests(callback){															//function to find all leave request along with type of leaves by making a join between leave_request and type_of_leave table
			app.models.LeaveRequest.find({
			where: {and: [{employeeId:id}, {leaveRequestYear:currentYear}, {status: {nin: [cancelStatus,deniedStatus]}}]},
			order: 'fromDate ASC',
			include: {relation: 'typeOfLeaves'}
			}, function(err, allLeaveRequests){
				if(err){
					return callback(err);
				}
				else{
					callback(null, allLeaveRequests);
				}
			});
		},

		function outputData(allLeaveRequests, callback){
			util.ErpKeysCode("leave_status", null, function(err, keys){								//to get all the possible status values for leaves
				if(err){
					return callback(err);			
				}
				var data = {};
				allLeaveRequests.forEach(function(leave){
					keys.forEach(function(key){
						if(leave.status === key.keyCode){
							var leaveStatus = key.keyValue;
							data = {};															//creating a JSON formatted data
							data.fromDate = leave.fromDate;
							data.toDate = leave.toDate;
							data.numberOfdays = leave.numberOfDays;
							data.typeOfLeave = leave.typeOfLeaves().leaveType;
							data.status = leaveStatus;
							data.description = leave.description;
							data.leaveRequestId = leave.id;
							data.halfDaySlot = leave.halfDaySlot;
							leaveData.push(data);
						}
					});
				});
				callback(null);
			});
		}

		],
		function(err, result){																		//returning final result to the calling function
			if(err){
				return cb(err);
			}
			else{
				cb(null, leaveData);
			}
		}
	);
};


exports.getList = getList;