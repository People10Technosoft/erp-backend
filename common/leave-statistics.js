var async = require('async');
var util = require('./utilities.js');
var moment = require('moment');
require('moment-weekday-calc');

var leaveStatistics = function(id, cb){												//function to get the leave statistics of an employee
	var currentDate = new Date();
	var currentYear = currentDate.getFullYear();
	var cancelStatus;
	var deniedStatus;
	var eligible_leave_count;

	async.waterfall([
	function cancelStatusValue(callback){											//function to get the cancel leave status
		util.ErpKeysCode("leave_status", "Cancelled", function(err, res){
			if(err){
				callback(err);
			}
			else{
				cancelStatus = res.keyCode;
				callback(null);
			}
		});
	},

	function deniedStatusValue(callback){											//function to get the denied status
		util.ErpKeysCode("leave_status", "Denied", function(err, res){
			if(err){
				callback(err);
			}
			else{
				deniedStatus = res.keyCode;
				//console.log("deniedStatus",deniedStatus);
				callback(null);
			}
		});
	},
	function findEligibleLeaveCount(callback){
		app.models.EmployeePrivilegeLeaveEligibility.findOne({where: {employeeId: id}},function(err, leaveCount){
			if(err){
				callback(err);
			}
			else if(leaveCount === null){
				callback("No Privilege Count");
			}
			else{
				console.log("leaveCount", leaveCount.eligibility);
				eligible_leave_count = leaveCount.eligibility;
				callback(null);
			}
		});
	},

	function findLeaveRequests(callback){											//function to find all leave request along with type of leaves by making a join between leave_request and type_of_leave table
		app.models.LeaveRequest.find({
		where: {and: [{employeeId:id}
		,{leaveRequestYear:currentYear}
		,{status: {nin: [cancelStatus, deniedStatus]}}]} 
		,include: {relation: 'typeOfLeaves'}
		},function(err, leaves){
			if(err){
				callback(err);
			}
			else{
				callback(null, leaves);		
			}
		});
	},

	function leaveStatisticsData(leaves, callback){									//function to fetch out the leave statistics of a particular employee
		util.ErpKeysCode("leave_status", null, function(err, response){
			if(err){
				callback(err);
			}
			var count_pending = 0;
			var count_approved = 0;
			var count_remaining = eligible_leave_count;
			var count_privilege = 0;
			var count_lop = 0;
			var count_total = eligible_leave_count;
			var count_toBeTaken = 0;
			var data = {
	 			pending : count_pending ,
	 			LOP: count_lop ,
	 			privilege: count_privilege ,
	 			remaining: count_remaining ,
	 			availed: (count_approved - count_toBeTaken) ,
	 			totalLeaves: count_total ,
	 			toBeTaken: count_toBeTaken
	 			};
			if(leaves.length < 1){
				return callback(null,data);
			}
			leaves.forEach(function(leave){												//to loop through all the leaves to find the different parameters of leave statistics
				response.forEach(function(res){
					if(leave.status === res.keyCode){
						if(res.keyValue === "Pending"){
							count_pending+=leave.numberOfDays;
						}
						else if(res.keyValue === "Approved"){
							count_approved+=leave.numberOfDays;
							if(moment(currentDate).isBefore(leave.fromDate)){
								count_toBeTaken+=leave.numberOfDays ;
							}
						}
					}
				});
				if(leave.typeOfLeavesId === leave.typeOfLeaves().id)
				{
					if(leave.typeOfLeaves().leaveType === "Privilege"){
						count_privilege+=leave.numberOfDays;
						//console.log("privilege:-",count_privilege);
					}
					if(leave.typeOfLeaves().leaveType === "Loss of Pay"){
						count_lop+=leave.numberOfDays;
					}
				}			
			});
			//console.log("privilege:-",count_privilege);								//creaing JSON (Data to be passed)
			count_remaining =  count_remaining - (count_privilege);
			data = {};
	 		data.pending = count_pending;
	 		data.LOP = count_lop;
	 		data.privilege = count_privilege;
	 		data.remaining = count_remaining;
	 		data.availed = (count_approved - count_toBeTaken);
	 		data.totalLeaves = count_total;
	 		data.toBeTaken = count_toBeTaken;
	 		callback(null,data);
		});
	},

	function specialLeavesTaken(data, callback){									//function to get the special leave statistics of an employee
		app.models.EmployeeSpecialLeaveEligibility.find({
		where: {and: [{employeeId: id},{status: 1}]}
		,include: ['typeOfLeaves','leaveRequest']}, function(err, leaves){
			if(err){
				return callback(err);
			}
			else{
				callback(null, leaves, data);
			}
		});
	},

	function leaveStatisticsWithSpecialLeavesIfAny(leaves, data, callback){
		var count_compoff = 0;
		var count_paternity = 0;
		var count_maternity = 0;
		if(leaves.length > 0){
		leaves.forEach(function(leave){
			if(leave.typeOfLeaves().leaveType === "Comp-off"){
				count_compoff+=1;
			}
			else if(leave.typeOfLeaves().leaveType === "Paternity"){
				data.paternity = leave.leaveRequest().numberOfDays ;
			}
			else if(leave.typeOfLeaves().leaveType === "Maternity"){
				data.maternity = leave.leaveRequest().numberOfDays ;
			}
		});
		if(count_compoff > 0){
			data.compOff = count_compoff ;
		}
		callback(null, data);
		}
		else{
			callback(null, data);
		}
	}

	],

	function(err, data){																//returning final result to the calling function
		if(err){
			cb(err);
		}
		else{
			console.log("data", data);
			cb(null,data);
		}
	});
};

	exports.leaveStatistics = leaveStatistics;