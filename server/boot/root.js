module.exports = function(server) {
  // Install a `/` route that returns server status
  var router = server.loopback.Router();
  var async = require('async');
  var ejs = require('ejs');
  var fs = require('fs');
  var util = require('../../common/utilities.js');
  var specialLeaves = require('../../common/leave-options.js');
  var summary = require('../../common/leave-statistics.js');
  var actionTaken = require('../../common/manager-actions.js');

  router.get('/', server.loopback.status());

  router.get('/date', function(request, response){  	                                           //get request to get to-date field from from-date and to-date
  	var fromDate = new Date(request.query.fromDate);                                             //or to get number of days field from from-date and to-date
  	var toDate = new Date(request.query.toDate);
  	var numberOfDays = request.query.numberOfDays ;
  	util.date(fromDate,toDate,numberOfDays, function(err, res){
  			response.send(res);
  	});
  });

  router.get('/specialLeaveDateCalculation', function(request, response){                         //date calculation same as above but here date calculation will be done 
    var fromDate = new Date(request.query.fromDate);                                              //including holidays as these are special leaves
    var toDate = new Date(request.query.toDate);
    var numberOfDays = request.query.numberOfDays ;
    util.specialLeaveDateCalculation(fromDate,toDate,numberOfDays, function(err, res){
        response.send(res);
    });
  });

  router.get('/specialLeaves', function(request, response){                                       //get request to get the special leaves wrt to an employee
    var id = request.query.id ;
    specialLeaves.leaveOptions(id, function(err,res){
      response.send(res);
    });
  });

  router.get('/leaveStatistics', function(request, response){                                      //get request to get the leave statistics of an employee
    var employeeId = request.query.id ;
    summary.leaveStatistics(employeeId, function(err,res){
    response.send(res);
    });
  });


  router.post('/managerActions', function(request, response){                                      //post request for manager actions like approve,deny or send-back
    var actions = request.body ;
    actionTaken.managerActions(actions, function(err,res){
    response.send(res);
    });
  });

  /*
  Data to be send for user login
  {
    "email":"erp4people10@gmail.com",
    "password":"people10"
  }
  */


  router.post('/login', function(request, response){                                               //post method for login 
    var req = request.body;                                                                        //an employee can login using his credentials only
    console.log("body",req);
 	  var data = {};

 	  async.waterfall([
 	  function employeeLogin(callback){                                                              //function for employee login(it gives an access token)
 	  	app.models.Employee.login({
      email: req.email,
      password: req.password
      },'employee', function(err, token){
      	if (err) {
          console.log("asdasd",err);
      		return callback(err);
      	}
        else{
      	  console.log(token);
      	  data.token = token ;
      	  callback(null);
        }
      });
 	  },

 	  function loginStatus(callback){                                                                 //function to maintain the login status                             
      util.loginStatus(data.token.userId, function(err, status){
        if(err){
 				return callback(err);
 			  }
 			  if(status === 0){
 				 data.freshLogin = 1;
        }
 			  console.log("status", status);
 			  callback(null);
      });
 	  },

 	  function isEmployeeManager(callback){                                                           //function to determine whether an user is manager or not
 	    util.managerList(function(err, managerList){
        if(err){
 			    return callback(err);
        }
        var employeeId = data.token.userId;
        var isManager = managerList.find(function(element){
          return (element.reportingManagerId === employeeId.toString());
 			  });
        if(isManager!== undefined){
          data.manager = 1;
        }
        console.log("managerList", managerList,isManager);
        callback(null);
      });
 	  },

    function isEmployeeALastLevelManager(callback){                                                 //function to check whether an employee is a highest level manager(management) or not
      var employeeId = data.token.userId;
      app.models.employee.findOne({where: {id: employeeId}}, function(err, response){
        if(err){
          return callback(err);
        }
        if((response.emp_id === "P10E0001") || (response.emp_id === "P10E0002")){
          data.lastLevelManager = 1;
        }
        data.name = response.name;
        callback(null);
      });
    }
 	  ]
 	  ,function(err, res){                                                                            //function returning the results 
      if(err){
 			  response.sendStatus(404);
      }
      else{
        console.log("****", data);
        response.send(data);
      }
 	  });
 	
  });



  router.post('/reset-password', function(req, res, next){                                           //function to reset password
    console.log("Reset Password Data:-", req);
    if(!req.accessToken){
      return res.sendStatus(401);
    }
                                                                                                      //verify passwords match
    if (!req.body.password ||
        !req.body.confirmation ||
        req.body.password !== req.body.confirmation){
      return res.sendStatus(400, new Error('Passwords do not match'));
    }

    app.models.Employee.findById(req.accessToken.userId, function(err, user){                         //query to find the user details
      if(err){
        return res.sendStatus(404);
      }
      user.updateAttribute('password', req.body.password, function(err, user){                        //query to update the user password
        if(err){
          return res.sendStatus(404);
        }
        console.log('> password reset processed successfully');
        res.send("Password Changed Successfully");
      });
    });
  });

  server.use(router);
};
