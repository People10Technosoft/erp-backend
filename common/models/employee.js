var loopback = require('loopback');
  var async = require('async'); 
module.exports = function(Employee) {


  //var util = require('../utilities.js');

  Employee.afterRemote('**',function(context, user, next){   
    console.log("-----------------------------",context.methodString);
    next();
  });

  Employee.on('resetPasswordRequest', function(info) {                                //Function to send password reset link to the email id
    var url = 'http://10.0.1.84:9000/#/password_reset';
    var html = 'Click <a href="' + url + '?id=' +info.accessToken.userId + '&access_token=' + info.accessToken.id + '">here</a> to reset your password';
    //'here' in above html is linked to : 'http://<host:port>/reset-password?access_token=<short-lived/temporary access token>'
    
    app.models.Email.send({                                                  //sending email 
      to: info.email,
      from: info.email,
      subject: 'Password reset',
      html: html
    },function(err){
      if (err){
        return console.log('Error sending password reset email');
      }
      else{
        console.log('> sending password reset email to:', info.email);
      }
    });
  });



  Employee.getApprovars = function(id, cb){                                           //Function to get the approvers for a particular employee

    // async.waterfall([
    // function approverList(clb){
    Employee.findOne({where: {id: id}
    },function(err, emp){
        if(err){
          return cb(err);
        }
        var data = {};
        var approvers = [];
        // if(emp === null)
        //   return cb("Invalid Employee Id");
        var level = emp.levelOfApprovalRequired;
        var managerId = emp.reportingManagerId;
        console.log("approval",level,managerId);
        if(level){
        async.whilst(                                                                 //while-loop to find the list of approvers 
        function(){
          if(level > 0){
            return true;
          }
        },

        function(callback){
          Employee.findOne({where: {id: managerId}
          },function(err, res){
            if(err){
              return callback(err);
            }
            // if(res === null){
            //   return callback("Invalid Manager Id");
            // }
            data = {};
            data.name = res.name ;
            data.id = res.id;
            approvers.push(data);
            managerId = res.reportingManagerId;
            level--;
            callback(null,approvers);
          });
        },

        function(err,data){
          if(err){
            return cb(err);
          }
          else{ 
            console.log("list of managers",data);
            cb(null, data);
          }
        }
      ); 
      }
      else{
        console.log("list of managers",data);
        cb(null, data);
      }     
    });
  // }
  // ]
  // ,function(err, data){
  //   if(data === undefined || data.length < 1){
  //     return cb(null, "NO Approvers:You Are A High Level Manager");
  //   }
  //   else{
  //     cb(null, data);
  //   }
  // });
};


Employee.remoteMethod('getApprovars',                                                 //Registering the getApprovers Method in the list of Api's
    {
      http :{path:'/getApprovars', verb: 'get'},
      accepts:{arg:'id', type: 'string', required: true},
      returns: {type: 'string', root:true}
  
    }
);

/*
Employee.currentUser = function(callback){                                            //Function to identify the current user
util.userIdentification(function(err, response){
  if(err)
    return callback("Unauthorized User");
console.log("user", response);
callback(null, response);
});
}

Employee.remoteMethod('currentUser',         
    {
      http :{path:'/user', verb: 'get'},
      //accepts:{arg:'id', type: 'string'},
      returns: {type: 'string', root:true}
  
    }
);
*/


/*
Data needs to be send:-
  {
    "id":"employeeId",
    "password":"people10"
  }
*/


Employee.beforeRemote('prototype.updateAttributes',function(context, user, next){     //After the first time login we need to change the login status
  var req = context.req.body;
  Employee.findById(req.id, function(err, user){                           //to find the employee data so that we can compare b/w new password and default password given by admin 
      if (err){
        return next(err);
      }
      user.hasPassword(req.password, function(err, isMatch){
        if(err){
          return next(err);
        }
        if(isMatch){
          next("New Password cannot be same as Old Password:Please type a different password");
        }
        else{
          user.updateAttribute('loginStatus', 1, function(err, user){
            if(err){ 
              return next(err);
            }
            else{
              next();    
            }
          });        
        }
      });
    });
});




Employee.afterRemote('logout', function(context, output, next){                        //Output: Message after the logout
  context.result = "Successfully Logged Out";
  next();
});

};
